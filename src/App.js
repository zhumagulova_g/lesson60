import React, {useState} from 'react';
import './App.css';
import Messages from "./Messages/Messages";
import {useEffect} from  'react';
import SendIcon from '@material-ui/icons/Send';
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button';
import axios from "axios";

const App = () => {
    const [messages, setMessages] = useState(
        []);
    const [inputMsg, setInputMsg] = useState('');
    const [lastDate, setLastDate] = useState('');
    let interval = null;
    const url = 'http://146.185.154.90:8000/messages';

    useEffect(() => {
        axios.get(url).then(function (response){
            const resData = response.data;
            const lastDateValue = resData.pop();
            setLastDate(lastDateValue.datetime);
            setMessages(resData);
        }).catch(function (error){
            console.log(error);
        }).then(function(){
        })
    },[])

    useEffect(() => {
        interval = setInterval(() => {
            const getMessages = async() => {
                try {
                    axios.get(url + '?datetime=' + lastDate).then(function (response){
                        const resData = response.data;
                        const messagesCopy = [...messages];
                        const newMessages = messagesCopy.concat(resData);
                        setMessages(newMessages);
                    }).catch(function (error){
                        console.log(error);
                    }).then(function(){
                    })
                } catch (e) {
                    console.log('Error:', e);
                }
            }
            getMessages().catch(error => console.log(error));
        }, 3000);
    },[])

    const changeInput = (e) => {
        e.preventDefault();
        const message = e.target.value;
        setInputMsg(message);
    };

    const postRes = () => {
        const data = new URLSearchParams();
        data.set('message', inputMsg);
        data.set('author', 'Gulzhamal');
        return axios.post(url, data
        ).then(function (response){
          console.log(response);
      }).catch(function (error){
          console.log(error);
      })
    };

  return (
      <div className="App">
          <div className='show-msg'>
                {messages.map(message  => (
                    <Messages
                        value={message.message}
                        key={messages.id}
                        author={message.author}
                        datetime={message.datetime}
                    />
                ))}
          </div>
          <div className="send-messages">
          <span className="author">Gulzhamal Zhumagulova</span>
          <TextField id="outlined-basic"
                     label="Write message"
                     variant="outlined"
                 className="input-class"
                 value={inputMsg}
                 onChange={e => changeInput(e)}/>
          <Button variant="contained"
                  color="primary"
                  onClick={postRes}
          >
              <span className="btn">Send message </span><SendIcon/>
          </Button>
      </div>
      </div>
  );
};

export default App;
