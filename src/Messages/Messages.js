import React from 'react';
import {Card} from "@material-ui/core";
import CardContent from '@material-ui/core/CardContent';
import moment from 'moment';

const Messages = (props) => {
    return (
        <Card className="message-class"
               variant="outlined"
        >
            <CardContent className="card">
                {moment(props.datetime).calendar()} {props.author}
            </CardContent>
            <CardContent className="card">
                {props.value}
            </CardContent>
        </Card>
    );
};

export default Messages;